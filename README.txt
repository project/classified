# Drupal Classified Module

This module adds colored banner to the top and bottom of site indicating the
Department of Defense level of content classification.

The three available levels are:
1.  Unclassified (Green)
2.  Classified (Yellow)
3.  Top Secret (Red)

*This module is under development. DO NOT use it on a productions site yet!*

## Installation

1.  Simply extract the module to wherever you install contributed modules. 
Generally /sites/all/modules
2.  Enable it 
3.  On the adminstration page at /admin/settings/classified set the 
classification level for your site.

## Enable the Module
Visit Admin > Build > Modules and enable the module.

## Configuration

After installing and enabling the module, visit 
Admin > Site Configuration > Site Classification Level to select the 
classification level for your site. 

By default after installation, the classificaiton level is unclassified.

Select your desired level and save the form.

If you don't see your results immediately, trying clearing your site's cache.
